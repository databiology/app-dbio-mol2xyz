# Mol2XYZ app

CIAO application of the mol2xyz code by Yuliia Orlova.

Input is a *mol file, output is a *xyz file (automatically generated from the input)

CIAO full documentation https://learn.ciao.tools/
