#
#   Mol2XYZ Application Dockerfile
#

# app/dbio/base:4.2.3 is based on Ubuntu 18.04
FROM app/dbio/base:4.2.3

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN apt update && \
    apt install -y python-rdkit && \
    rm -rf /var/lib/apt/lists/*

# Adding python script
COPY mol2xyz.py /usr/local/bin/mol2xyz.py

# Entrypoint is defined as /usr/local/bin/main.sh in app/dbio/base:4.2.3
COPY main.sh  /usr/local/bin/main.sh
RUN  chmod +x /usr/local/bin/main.sh
