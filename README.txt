mol2xyz script converts MDL mol file in xyz file. MDL mol file produced from molecular graph usually doesn't have coordinated. The script optimises molecule's geometry and generates xyz file that can be viewed with Avogadro (for example).  

The code takes adjacency list from the input file and produces two output files that describe a molecule as a molecular graph: list of atoms and adjacency matrix.

RDKit environment has to be installed, then to run the code:

source activate my-rdkit-env
python mol2xyz.py 'EL.mol' 'EL.xyz'


'EL.mol' - input file in MDL mol format
'EL.xyz' - output file in xyz format