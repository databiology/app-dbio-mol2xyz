from __future__ import print_function
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Draw
from rdkit.Chem import Descriptors
import sys, os, argparse

#  Created by Yuliia Orlova on 01/07/19.
#  Copyright 2019 Yuliia Orlova. All rights reserved.


mol_file = sys.argv[ 1 ]
adf_in_file = sys.argv[ 2 ]

mol = Chem.MolFromMolFile(mol_file, removeHs = False, strictParsing=False) 
#AllChem.EmbedMolecule(mol, AllChem.ETKDG()) ## AllChem.ETKDG method is not defined in AllChem - JC ##
AllChem.EmbedMolecule(mol)
AllChem.MMFFOptimizeMolecule(mol)
hmol = mol
AllChem.EmbedMolecule(  hmol, useExpTorsionAnglePrefs=True, useBasicKnowledge=True ) 
atoms = [ atom for atom in hmol.GetAtoms() ]
def atomposition2string( atom ):
    line = "{} {} {} {}"
    conf = atom.GetOwningMol().GetConformer()
    posi = conf.GetAtomPosition( atom.GetIdx() )
    line = line.format( atom.GetSymbol(), posi.x, posi.y, posi.z )
    line +="\n"
    return line


molstring ="%d\n\n" % len(atoms)
  
outf = open(adf_in_file, 'w')
for atom in atoms:
    l=atomposition2string(atom)
    molstring += l
outf.write( molstring )
outf.close()
