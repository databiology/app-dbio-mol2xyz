#!/bin/bash

# Mol2XYZ main script

# Global variables definition
SCRATCH=/scratch
RESULTSDIR="$SCRATCH/results"
INPUTDIR="$SCRATCH/input"
SCRIPT=/usr/local/bin/mol2xyz.py

# functions
falseexit() {
   echo "Application failed: $1"
   exit 1
}

# Perform task over all input files
IFS=$'\n'
for FILE in $(jq -r '.resources[].relativePath' "$SCRATCH/projection.json")
do
    INFILE="$INPUTDIR/$FILE"
    if [ -f "$INFILE" ]
    then
        echo "Converting $INFILE"
    else
        falseexit "$INFILE is missing"
    fi

    OUTFILE="$RESULTSDIR/$(basename $INFILE .mol).xyz"
    CMD="python '$SCRIPT' '$INFILE' '$OUTFILE'"
    echo "Executing: $CMD"
    bash -c "$CMD" || falseexit "Error converting file"
done